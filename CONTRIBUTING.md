# Contributing Guidelines

## Quick gist of Rust (no pun intended)
https://fasterthanli.me/articles/a-half-hour-to-learn-rust

## What we want users to see
- We want to provide a zipped directory
- Head TA will set everything up at the very beginning of the semester (assignment configs, gitlab group ids, etc.)
- Each TA will simply add their name, credentials, and their student list
- Grader will run automated tests, and TAs can see output and add their own comments
- Then they will run the grading binary with the `--post` option to push scores with comments

## Notes
- Try to structure your code so that Rust compiler can perform maximum static analysis on your logic's correctness
	- avoid `unreachable!()`
	- `assert!()` exists, but it is usually possible to avoid the need for "assumption" by having compiler ensure your logic with pattern matches (see error handling section below)
- Because we provide all the config and directory with the distribution prepopulated, we don't need to account for aliases, etc.
- Keep things simple, and ensure they do one thing well.
	- If you're unsure, add unit tests
		- example of simple test in `mod tests` in `src/yaml.rs`
		- example of async test in `mod tests` in `src/drivers/git.rs`
- Never fail silently - if you reach a unrecoverable state, print out what went wrong.
- Never panic outside of `main.rs`. `main.rs` is the only place where things can panic.
	- For handling errors anywhere else, use `?` chaining with `anyhow::Result`
- Focus on reproducibility and reliability - avoid sleeps and timers as much as possible to prevent flakiness.
- Preserve feature-rich types as much as possible - if something is a `PathBuf`, don't convert it to `String` until the very part where you print.
	- On the contrary, if you get a path as a String, convert it to `PathBuf` as soon as possible (don't forget sanity checks!)
	- For `Path` and `PathBuf`, the `.display()` method allows infallable conversion to `String` (as opposed to `.to_string()`). This is due to Rust `String`s always being valid UTF-8, unlike `OsString` which `Path` uses.
- Don't juggle around parameters - if your function is getting more than 5 inputs, that probably means that you need to abstract it into some sort of an object

## Details on Error Handling
Say you have a fallable operation `fn do_something() -> Result<i32, SomeError>`.

- `do_something().unwrap()` should NEVER be used.
- `do_something().expect("helpful_message")` is accepted, but only in `main.rs`
- In everywhere else, you want to do the following:
```rust
use anyhow::anyhow;

// typedef for error_stack::Result<T, anyhow::Error>
use crate::utils::Result;
// macro for `std::result::Result` types,
// which convert it to error_stack::Result<T, anyhow::Error>
// cannot be a function/utils, since the purpose of `error_stack::Result<>` is to
// capture the environment when the error happened (for easier debug)
// which requires the conversion to happen in the function where the error happens
use crate::utils::report;

fn my_function() -> Result<i32> // note the `Result` here is `crate::utils::Result` (which is a typedef for error_stack::Result<T, anyhow::Error>), not std::result::Result - we shadowed the default import above
{
	let i: i32 = report!(do_something())?; // Our macro that converts to error_stack::Result<i32, anyhow::Error>, where anyhow::Error Boxes any error type to anyhow::Error - then the `?` operator punts (throws) error up if it is an error, and `.unwrap()`s if it is Ok, returning `i32`.
	Ok(i + 1) // returns i + 1
}
```

### What if I do want to detect error and do something?
- If you want to have a default value when something fails, do:
```rust
let i = do_something().unwrap_or(42);
// unwrap_or() is ALWAYS evaluated, so unwrap_or_else() that gets a closure is encouraged for nonimmediate default values
let s = do_something_str().unwrap_or_else(|| "fourty-two".to_string());
```
- Use pattern matching - notably `if let` and/or `match`:
```rust
// I only care if it's an error - if it succeeded, ignore
if let Err(e) = do_something() 
{
	// do something with your error
}
else {} // if-let-else is possible, but `match` is encouraged over `if-let-else`

// I only care if it's not an error - if it failed, ignore
if let Ok(v) = do_something()
{
	// do something with your return value
}

// I want to have custom handling for both conditions
match do_something()
{
	Ok(v) => 
	{
		// do something with `v`
	}
	Err(e) =>
	{
		// do something with `e`
	}
}
```

## Style Guideline
Do:
```
cargo +nightly fmt
```
for formatting,

and ensure:
```
cargo clippy
```
produces no warnings before committing.

## I need to get some user input, where should I put it?
Best input is no input - if existing inputs can be used for your needs, or if you can compute your desired value with existing known variables, then don't add them.
> Example: TA name is unnecessary, since it can be substituted with TA's directory ID, which can be fetched from gitlab account name

Also, avoid providing too much toggles - it's often good enough to have sane defaults and readable error messages.

If above is not the case, you should add them in the following locations:
- If a value can vary run-by-run: `args.rs`
	- example: assignment ID to grade, `grade` mode vs. `post` mode
- If a value can vary assignment-by-assignment: `assignment.yaml` and `test.yaml`
	- example: container structure, grading point distribution, etc.
- If a value can vary TA-by-TA: `credentials.yaml`
	- example: GitLab PAT, TA's directory id, etc.
- If a value can vary course-by-course or semester-by-semester: `config.yaml`
	- example: Gitlab group id, Gitlab instance URL, etc.

## Organization
### main.rs
- Driver, all errors are handled here
### args.rs
- Args parser and args sanity checker
- `Args` struct is raw args (in `String`), `ParsedArgs` are actual useful data
	- i.e. if `Args` gets `config.yaml`'s path as a string, `ParsedArgs` will have actual parsed structure of that yaml.
	- `ParsedArgs` take ownership of `Args`, so caller doesn't access it anymore. If program ever needs to access raw path or something, simply add it to `ParsedArgs`.
### types.rs
- We want to minimize this file as much as possible
- Custom types that doesn't really fit anywhere else goes here.
	- Currently wrapper type `Assignment` that contains `AssignmentConfig` and parsed `tests.yaml` files.
### utils.rs
- We want to minimize this file as much as possible
- Custom helper trait for chrono::DateTime<Utc> that allows adding it with std::time::Duration
### yaml.rs
- YAML definitions and custom parsers associated with it
### `drivers/`
- `git.rs`: Git driver, each instance of `struct Git` is tied to single repository
- `docker/`: Docker driver, see below
	- `mod.rs`: stateless docker operations (load image, list images, etc)
	- `network/`: abstraction over a single docker network
		- `mod.rs`: contains mappings to docker network operations
		- `helper.rs`: contains helpers for `tcpdump`
	- `container/`: abstraction over a single container
		- `mod.rs`: contains mappings to docker container operations
		- `helper.rs`: contains helpers for more complex operations (poll until status, get network info)
		- `types.rs`: internal type definitions used in `helper.rs` 
### `gitlab/`
- `mod.rs`: Contains generic REST HTTP client (`struct GitLab`) for GitLab
- `helper.rs`: Contains trait `GitlabClientHelpers` that adds useful functions to raw `Git:ab` struct.
### `repo/`
- Abstraction for student Git repository, uses `drivers/git.rs` under the hood
- Each instance of `struct Repo` contains all information necessary to perform grading
- `struct Repo` has two core abstractions: `run_tests()` and `grade()` - see `grade.rs`
