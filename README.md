# test_runner

For `.yaml` files that are not listed below, refer to the `_example_<filename>.yaml` variants on the repository.

Make sure to `docker image rm instructor-cmsc417fall22image:latest` and download [this](https://umd.box.com/s/m6jlm9ni28zg2vvtyksdfmg6m8qutoxe) as `instructor-cmsc417fall22image.tar` (it's a tarball that has been tgz-ed, so use the inner tar) referred by `config.yaml`!

> See [CONTRIBUTING.md](https://gitlab.com/chocological00/test_runner/-/blob/master/CONTRIBUTING.md) before editing code

## Directory Structure
`>` is directory, `-` is file:

```
- test_runner.elf
> assignments
  > a0
    > t0
      - container-config.yaml
    > t1
      - container-config.yaml
    - assignment.yaml
    - grade.py
  > a1
    > t0
      - container-config.yaml
    - assignment.yaml
    - grade.py
> reference
  > a0
    - source.c
    - Makefile
- config.yaml
- credentials.yaml
- students.tsv
- docker-image.tar
```

## File Structure
### `container-config.yaml`
- Dynamically resolved variables are in syntax `${<container-name>:<var>}`
- `var` can be `ipv4`, or `mac`

```yaml
# TCPDUMP comes with caveats on Docker for Windows, see below section
tcpdump: False
# set to true for P2P, false for server-client model
# even if this key is true, persistent containers will always be launched before non-persistent ones (this is due to docker's network address resolving limitations)
simultaneous_launch: True
# test won't run if these files don't exist on repo after building
expected_artifacts: ["rt"]
# container layout - if simultaneous_launch is False, containers are launched in order
layout:
  # container name
  node0:
    # whether to run reference code or not - UB if no_reference (in assignment.yaml) and reference is both True
    reference: False
    # whether repository should be mounted as read_only
    read_only: True
    # additional bind mounts (i.e. for config files)
    # if unnecessary, leave it empty: `[]`
    # Tuple of (host_path_relative, container_path, read_only)
    additional_mounts:
      - - "config"
        - "/config"
        - false
    # command to launch
    #
    # dynamically resolvable - do ${container_name:key}
    # key can be `name` for container name, `ipv4` for ipv4 address, or `mac` for mac address
    # `ipv4` and `mac` can ONLY be used in non-persistent containers to resolve persistent containers
    #
    # if you want to use `&&`, do:
    # launch_cmd:
    #   - "sh"
    #   - "-c"
    #   - "stdbuf -i0 -o0 -e0 ./my_cmd && stdbuf -i0 -o0 -e0 ./another_cmd -n ${node0:name}"
    # stdbuf prepending is necessary when using `sh -c` - otherwise no output will be captured
    launch_cmd: ["./rt", "-n", "0"]
    # timeout in seconds
    # if <= 0, container is considered to be "persistent"
    timeout: 30
```

### `assignment.yaml`
- Due is in ET (DST automatically calculated)

```yaml
# <this>.git of the clone URL
repo_name: "a1"
due: "2022-09-25 23:59:59"
# won't build reference if true
no_reference: False
# ran on /root of docker, which is mounted as git repo root of each student
build_command: ["make"]
```

## TCPDUMP on Docker for Windows
Packet capturing on the Docker virtual network currently relies on `nsenter`-ing into Docker overlay network.

This works on Linux and macOS (since Docker for macOS is simply running on a Linux VM), but has problems with Windows, as Docker on Windows uses Hyper-V networking, rather than Linux virtual network stack.

To fix this issue, you need to run Linux-native Docker on WSL, rather than using Docker Desktop for Windows's WSL integration.

You must use WSL2. Follow the steps below:

1. Quit Docker Desktop for Windows. You don't need to uninstall it.
2. Enable systemd for WSL2.
    1. In your WSL2, modify `/etc/wsl.conf` to contain the following:
        > ```
        > [boot]
        > systemd=true
        > ```
    2. Save and exit. Reboot your WSL by doing `wsl --shutdown` on PowerShell.
3. Install Linux-native version of Docker on WSL2. For Ubuntu, this can be done with `sudo apt update && sudo apt install docker.io`
4. Disable docker service autostart on WSL2 to prevent possible conflict with Docker Desktop for Windows: `systemctl disable docker`
5. Start the Docker service manually: `systemctl start docker`
6. Put the Docker Linux Daemon into swarm mode: `docker swarm init`
7. Run the grader!

### Going back to Docker on Windows after above steps
1. Stop Linux native Docker service: `systemctl stop docker`
2. Launch Docker Desktop for Windows
3. Profit!

### Steps for subsequent runs (You only need to enable systemd / reinstall docker once)
1. Quit Docker Desktop for Windows
2. On WSL2, `systemctl start docker`
