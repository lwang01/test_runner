// program args parser, sanity checker, etc.
#[cfg(target_family = "unix")]
use std::os::unix::fs::PermissionsExt;
use std::{
	collections::BTreeMap,
	path::{Path, PathBuf},
	time::Duration
};

use anyhow::anyhow;
use clap::Parser;
use tokio::fs::{self, File};
use tokio_stream::{wrappers::ReadDirStream, StreamExt};

use crate::{
	report,
	types::Assignment,
	utils::Result,
	yaml::{AssignmentConfig, Config, ContainerConfig, Credentials}
};

// clap Args, DON'T USE THIS STRUCT DIRECTLY
// MUST FEED INTO ParsedArgs::new() TO SANITY CHECK AND TYPE COERCE!!!

/// 417 Distributed Code Tester
#[derive(Parser, Debug)]
pub struct Args
{
	#[clap(short, long, help = "Assignment to grade")]
	assignment: String,
	#[clap(
		short,
		long,
		default_value = "./config.yaml",
		help = "Configuration file location"
	)]
	config: String,
	#[clap(
		short = 'r',
		long,
		default_value = "./credentials.yaml",
		help = "Credentials file location"
	)]
	credentials: String,
	#[clap(
		long,
		help = "Path to assignments directory",
		default_value = "./assignments"
	)]
	assignment_path: String,
	#[clap(
		long,
		help = "Path to reference code",
		default_value = "./reference"
	)]
	reference_code_path: String,
	#[clap(
		short,
		long,
		help = "List of students to grade, in TSV",
		default_value = "./students.tsv"
	)]
	students_tsv: String,
	#[clap(short, long, help = "Directory to clone student repositories to")]
	workdir: String,
	#[clap(
		short,
		long,
		help = "Directory to store debug information about tests"
	)]
	debug_workdir: String,
	#[clap(long, help = "Max concurrent graders (default: CPU threads)")]
	max_concurrent: Option<usize>,
	#[clap(long, help = "Don't ask for confimation, I know what I'm doing")]
	no_confirm: bool,
	#[clap(
		long,
		help = "If specified, don't reset / checkout the student repos (i.e. grade based on what's currently there, HEAD if repo didn't exist and is cloned)"
	)]
	no_reset: bool,
	#[clap(
		long,
		help = "If specified, don't remove containers after the test is finished (will only stop them)"
	)]
	no_remove: bool
}

/// internal struct for sanity-checked args
#[derive(Debug)]
pub struct ParsedArgs
{
	// holder structure for assignment-related configuration, corresponding to `assignment` in `Args`
	pub assignment: Assignment,
	// parsed config file
	pub config: Config,
	// parsed Credentials file
	pub credentials: Credentials,
	// directory id of students and their extension amount
	pub students: BTreeMap<String, Option<Duration>>,
	// path to directory where student repos will be cloned to
	pub workdir: PathBuf,
	// path to directory where debug outputs will be stored
	pub debug_workdir: PathBuf,
	// max concurrency limit
	pub max_concurrent: Option<usize>,
	// flags, see above
	pub no_confirm: bool,
	pub no_reset: bool,
	pub no_remove: bool
}

impl ParsedArgs
{
	/// check args sanity (i.e. if path is provided, does it exist? etc.)
	/// and parse them into usable types (i.e. actual file contents parsed, etc.)
	/// takes ownership, so caller can't use unparsed args anymore
	pub async fn new(args: Args) -> Result<ParsedArgs>
	{
		// check fs entries
		let config: Config = report!(serde_yaml::from_str(&report!(
			fs::read_to_string(Path::new(&args.config)).await.map_err(|e| {
				anyhow!("Failed to read config file at {}: {}", args.config, e)
			})
		)?))?;
		if !Path::new(&config.docker_image_path).is_file()
		{
			return report!(Err(anyhow!(
				"Image file {} does not exist",
				config.docker_image_path
			)));
		}
		let credentials: Credentials = report!(serde_yaml::from_str(
			&report!(fs::read_to_string(Path::new(&args.credentials))
				.await
				.map_err(|e| {
					anyhow!(
						"Failed to read credentials file at {}: {}",
						args.credentials,
						e
					)
				}))?
		))?;
		let assignment_path =
			Path::new(&args.assignment_path).join(&args.assignment);
		let assignment_config: AssignmentConfig =
			report!(serde_yaml::from_str(&report!(fs::read_to_string(
				assignment_path.join("assignment.yaml")
			)
			.await
			.map_err(|e| {
				anyhow!(
					"Failed to read assignment config file at {}: {}",
					assignment_path.join("assignment.yaml").display(),
					e
				)
			}))?))?;
		let reference_code_path =
			Path::new(&args.reference_code_path).join(&args.assignment);
		// only check if there is reference code
		if !assignment_config.no_reference && !reference_code_path.is_dir()
		{
			return report!(Err(anyhow!(
				"reference code path is not a directory: {}",
				reference_code_path.display()
			)));
		}
		let students = parse_students_tsv(&report!(fs::read_to_string(
			Path::new(&args.students_tsv)
		)
		.await
		.map_err(|e| {
			anyhow!(
				"Failed to read students TSV file at {}: {}",
				args.students_tsv,
				e
			)
		}))?)?;
		let workdir = Path::new(&args.workdir);
		if !workdir.is_dir()
		{
			return report!(Err(anyhow!(
				"Directory {} does not exist",
				workdir.display()
			)));
		}
		let workdir = workdir.join(&args.assignment);
		if !workdir.is_dir()
		{
			report!(fs::create_dir(&workdir).await)?;
		}
		let debug_workdir = Path::new(&args.debug_workdir);
		if !debug_workdir.is_dir()
		{
			return report!(Err(anyhow!(
				"Directory {} does not exist",
				debug_workdir.display()
			)));
		}
		let debug_workdir = debug_workdir.join(&args.assignment);
		if !debug_workdir.is_dir()
		{
			report!(fs::create_dir(&debug_workdir).await)?;
		}

		let mut tests: BTreeMap<String, ContainerConfig> = BTreeMap::new();
		let read_dir: Vec<_> =
			ReadDirStream::new(report!(fs::read_dir(&assignment_path).await)?)
				.collect()
				.await;
		// for each subdirectory in assignment directory
		for entry in read_dir
		{
			let entry = report!(entry)?;
			if report!(entry.file_type().await)?.is_dir()
				&& !entry.file_name().to_string_lossy().starts_with('_')
				&& !entry.file_name().to_string_lossy().starts_with('.')
			{
				tests.insert(
					entry.file_name().to_string_lossy().to_string(),
					report!(serde_yaml::from_str(&report!(
						fs::read_to_string(
							entry.path().join("container-config.yaml")
						)
						.await
						.map_err(|e| {
							anyhow!(
							"Failed to read container config file at {}: {}",
							entry
								.path()
								.join("container-config.yaml")
								.display(),
							e
						)
						})
					)?))?
				);
			}
		}
		let grader_path = assignment_path.join("grade.py");
		if !grader_path.is_file()
		{
			return report!(Err(anyhow!(
				"File {} does not exist",
				grader_path.display()
			)));
		}
		#[cfg(target_family = "unix")]
		{
			let grader = report!(File::open(&grader_path).await)?;
			let grader_perms = report!(grader.metadata().await)?.permissions();
			if grader_perms.mode() & 0o111 == 0
			{
				return report!(Err(anyhow!(
					"File {} is not executable!",
					report!(grader_path.canonicalize())?.display()
				)));
			}
		}

		let assignment = Assignment {
			config: assignment_config,
			tests_path: assignment_path,
			reference_code_path,
			grader_path,
			tests
		};

		if let Some(max_concurrent) = args.max_concurrent
		{
			if max_concurrent < 1
			{
				return report!(Err(anyhow!(
					"max-concurrent must be at least 1, got {}",
					max_concurrent
				)));
			}
		}

		Ok(ParsedArgs {
			assignment,
			config,
			credentials,
			students,
			workdir,
			debug_workdir,
			// we don't handle cpu detection / avail ram detection here so we can use the latest RAM info
			max_concurrent: args.max_concurrent,
			no_confirm: args.no_confirm,
			no_reset: args.no_reset,
			no_remove: args.no_remove
		})
	}
}

/// tsv with comments parser
/// returns a BTreeMap of student ids to their extension amount
fn parse_students_tsv(tsv: &str) -> Result<BTreeMap<String, Option<Duration>>>
{
	let mut students: BTreeMap<String, Option<Duration>> = BTreeMap::new();

	for line in tsv.lines()
	{
		let line = line.trim();
		if line.starts_with('#') || line.is_empty()
		{
			continue;
		}
		if let Some((directory_id, extension_amount)) = line.split_once('\t')
		{
			let directory_id = directory_id.trim();
			let extension_amount = extension_amount.trim();
			if directory_id.is_empty()
			{
				return report!(Err(anyhow!(
					"Empty directory id in students.tsv"
				)));
			}
			if extension_amount.is_empty()
			{
				students.insert(directory_id.to_string(), None);
			}
			else
			{
				let extension_amount =
					report!(humantime::parse_duration(extension_amount)
						.map_err(|e| {
							anyhow::anyhow!("invalid extension amount {}", e)
						}))?;
				students
					.insert(directory_id.to_string(), Some(extension_amount));
			}
		}
		else
		{
			students.insert(line.to_string(), None);
		}
	}

	Ok(students)
}
