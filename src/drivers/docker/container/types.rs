use std::net::IpAddr;

/// network information for a container
pub struct NetworkInfo
{
	/// name of the network
	pub name: String,
	/// alias of current container on this network (DNS resolvable name)
	pub aliases: Vec<String>,
	pub gateway: IpAddr,
	pub ip_address: IpAddr,
	pub mac_address: String
}
