use std::{net::IpAddr, str::FromStr, time::Duration};

use anyhow::anyhow;
pub use bollard::{models::ContainerStateStatusEnum, Docker};

use super::types::NetworkInfo;
use crate::{report, utils::Result};

#[async_trait::async_trait]
pub trait DockerExt
{
	async fn is_swarm_mode() -> Result<bool>;
}

#[async_trait::async_trait]
impl DockerExt for Docker
{
	async fn is_swarm_mode() -> Result<bool>
	{
		let docker = report!(Docker::connect_with_local_defaults())?;
		let info = report!(docker.info().await)?;
		Ok(info.swarm.is_some())
	}
}

#[async_trait::async_trait]
pub trait ContainerExt
{
	async fn get_network_info(&self) -> Result<Vec<NetworkInfo>>;
	async fn poll_for_status_with_timeout(
		&self, status: ContainerStateStatusEnum, timeout: Duration
	) -> Result<bool>;
}

#[async_trait::async_trait]
impl ContainerExt for super::Container
{
	async fn get_network_info(&self) -> Result<Vec<NetworkInfo>>
	{
		let container = self.inspect().await?;
		let network_settings =
			report!(container.clone().network_settings.ok_or_else(|| {
				anyhow!("Container has no network settings: {:?}", &container)
			}))?;
		let networks =
			report!(network_settings.clone().networks.ok_or_else(|| {
				anyhow!("Container has no networks: {:?}", &network_settings)
			}))?;
		let mut network_infos = Vec::new();
		for (name, network) in networks
		{
			let gateway = report!(network.clone().gateway.ok_or_else(|| {
				anyhow!("Container has no gateway: {:?}", &network)
			}))?;
			let gateway = report!(IpAddr::from_str(&gateway))?;
			let ip_address =
				report!(network.clone().ip_address.ok_or_else(|| {
					anyhow!("Container has no ip address: {:?}", &network)
				}))?;
			let ip_address = report!(IpAddr::from_str(&ip_address))?;
			let mac_address =
				report!(network.clone().mac_address.ok_or_else(|| {
					anyhow!("Container has no mac address: {:?}", &network)
				}))?;
			let network_info = NetworkInfo {
				name,
				aliases: network.aliases.unwrap_or_default(),
				gateway,
				ip_address,
				mac_address
			};
			network_infos.push(network_info);
		}
		Ok(network_infos)
	}

	/// polls for a specific status with a timeout
	/// not very high precision (may wait longer), but will do it's job
	/// returns true if status was reached, false if timeout was reached
	/// negative or zero timeout will return instantly
	async fn poll_for_status_with_timeout(
		&self, status: ContainerStateStatusEnum, timeout: Duration
	) -> Result<bool>
	{
		let mut elapsed = Duration::from_secs(0);
		let mut interval = tokio::time::interval(Duration::from_millis(250));
		loop
		{
			let current_status = self.get_status().await?;
			if current_status == status
			{
				return Ok(true);
			}
			if elapsed >= timeout
			{
				return Ok(false);
			}
			// don't spin
			interval.tick().await;
			elapsed += Duration::from_millis(250);
		}
	}
}
