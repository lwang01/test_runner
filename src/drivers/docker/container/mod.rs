use std::{
	path::{Path, PathBuf},
	time::Duration
};

use anyhow::anyhow;
use bollard::{
	container::{
		Config,
		CreateContainerOptions,
		KillContainerOptions,
		LogOutput,
		LogsOptions
	},
	models::{
		ContainerInspectResponse,
		ContainerStateStatusEnum,
		HostConfig,
		RestartPolicy,
		RestartPolicyNameEnum
	},
	Docker
};
use futures::Stream;
use tokio_stream::StreamExt;

use super::network::Network;
use crate::{report, utils::Result};

pub mod helper;
pub mod types;

pub struct Container
{
	// for internal tracking of container id only
	// not to be used as a cache / direct access!
	container: ContainerInspectResponse
}

// constructors
impl Container
{
	/// create a container
	///
	/// will mount provided Path to `/root` and run provided command as entrypoint
	///
	/// will also attach to provided network
	///
	/// additional_mounts are Vec of (host_path, container_path, ro)
	///
	/// each container is limited to 2GiB of ram, ~~and has ulimit of 2048 to prevent forkbomb knocking down docker daemon~~
	///
	/// TODO: add function to optionally have additional mount under /root/config for config files they have to load
	pub async fn create(
		container_name: &str, image_name: &str, mount_path: &Path,
		read_only: bool, additional_mounts: Vec<(PathBuf, &str, bool)>,
		command: Vec<&str>, network: Option<&Network>
	) -> Result<Container>
	{
		let docker = report!(Docker::connect_with_local_defaults())?;

		let options = CreateContainerOptions { name: container_name };

		let network_mode = if let Some(network) = network
		{
			network.get_name()?
		}
		else
		{
			"none".to_owned()
		};

		let mut bind_vec = vec![format!(
			"{}:/root:{}",
			report!(mount_path.canonicalize())?.display(),
			if read_only { "ro" } else { "rw" }
		)];
		for (host_path, container_path, ro) in additional_mounts
		{
			bind_vec.push(format!(
				"{}:{}:{}",
				report!(host_path.canonicalize())?.display(),
				container_path,
				if ro { "ro" } else { "rw" }
			));
		}

		// bind-mount need to use `binds` field in `HostConfig` configuration struct
		// https://github.com/fussybeaver/bollard/issues/129
		let host_config = HostConfig {
			// 2GiB
			memory: Some(2 * 1024 * 1024 * 1024),
			// reap orphans
			init: Some(true),
			// // prevent forkbomb // FIXME: breaks when set
			// ulimits: Some(vec![bollard::models::ResourcesUlimits {
			// 	name: Some("nproc".to_string()),
			// 	hard: Some(65535),
			// 	..Default::default()
			// }]),
			binds: Some(bind_vec),
			// connect to our network
			network_mode: Some(network_mode),
			// port_bindings is used for binding the docker HOST port to the container port
			// so we don't need that here, since what happens in the container network stays in the container network
			// don't restart on failure
			restart_policy: Some(RestartPolicy {
				name: Some(RestartPolicyNameEnum::NO),
				..Default::default()
			}),
			// our code is responsible for removing containers, not docker daemon
			auto_remove: Some(false),
			..Default::default()
		};

		let config = Config {
			image: Some(image_name),
			working_dir: Some("/root"),
			entrypoint: Some(command),
			host_config: Some(host_config),
			..Default::default()
		};

		let res =
			report!(docker.create_container(Some(options), config).await)?;

		if !res.warnings.is_empty()
		{
			tracing::warn!(
				"A warning has occurred while creating a docker container {}: {}",
				container_name,
				res.warnings.join("\n\n")
			);
		}

		let container = report!(docker.inspect_container(&res.id, None).await)?;
		Ok(Container { container })
	}

	/// create a container from a inspect result - FOR USAGE IN NetworkExt ONLY!!!
	pub unsafe fn from_unchecked(
		container: ContainerInspectResponse
	) -> Container
	{
		Container { container }
	}
}

// operations
impl Container
{
	pub fn get_id(&self) -> Result<String>
	{
		report!(self.container.id.clone().ok_or_else(|| {
			anyhow!("Container has no id: {:?}", &self.container)
		}))
	}

	/// WARN: assumes container was initialized with some name!!!
	pub fn get_name(&self) -> Result<String>
	{
		report!(self
			.container
			.name
			.as_ref()
			.map(|s| s.trim_start_matches('/').to_string())
			.ok_or_else(|| {
				anyhow!("Container has no name: {:?}", &self.container)
			}))
	}

	pub async fn exists(&self) -> Result<bool>
	{
		let docker = report!(Docker::connect_with_local_defaults())?;
		let container_id = self.get_id()?;
		Ok(docker.inspect_container(&container_id, None).await.is_ok())
	}

	pub async fn inspect(&self) -> Result<ContainerInspectResponse>
	{
		let docker = report!(Docker::connect_with_local_defaults())?;
		let container_id = self.get_id()?;
		report!(docker.inspect_container(&container_id, None).await)
	}

	pub async fn get_status(&self) -> Result<ContainerStateStatusEnum>
	{
		let docker = report!(Docker::connect_with_local_defaults())?;
		let container_id = self.get_id()?;
		let container =
			report!(docker.inspect_container(&container_id, None).await)?;
		let state = report!(container.clone().state.ok_or_else(|| anyhow!(
			"Container has no state: {:?}",
			&container
		)))?;
		report!(state
			.status
			.ok_or_else(|| anyhow!("Container has no status: {:?}", &state)))
	}

	/// starts the container
	/// gets mutable reference to self to prevent potential race
	pub async fn start(&mut self) -> Result<()>
	{
		let docker = report!(Docker::connect_with_local_defaults())?;
		let container_id = self.get_id()?;

		let mut i = 0;
		let err = loop
		{
			let res = docker.start_container::<&str>(&container_id, None).await;
			if let Err(e) = res
			{
				if let bollard::errors::Error::DockerResponseServerError {
					status_code,
					..
				} = e
				{
					if status_code == 500 || status_code == 404
					{
						// attaching to network failed
						// happens when there's a high parallelism
						// retry a few, then give up
						if i > 5
						{
							break e;
						}
						else
						{
							i += 1;
							// linear backoff
							tokio::time::sleep(Duration::from_secs(i)).await;
							continue;
						}
					}
					else
					{
						break e;
					}
				}
				else
				{
					break e;
				}
			}
			else
			{
				return Ok(());
			}
		};

		report!(Err(err))?;
		Ok(())
	}

	/// sends SIGINT to container
	/// container must be initialized with `--init` option for this to work
	pub async fn sigint(&self) -> Result<()>
	{
		let docker = report!(Docker::connect_with_local_defaults())?;
		let container_id = self.get_id()?;
		let signal = KillContainerOptions { signal: "SIGINT" };
		report!(
			docker.kill_container::<&str>(&container_id, Some(signal)).await
		)?;
		Ok(())
	}

	// TODO: bool was added late, since I didn't realize that I can't stop a container that is already stopped
	// so go through all references of stop and see if such check will be relevant
	// i.e. no poll nearby

	/// stops the container
	///
	/// gets mutable reference to self to prevent potential race
	///
	/// returns true if stopped, false if already stopped
	pub async fn stop(&mut self) -> Result<bool>
	{
		let docker = report!(Docker::connect_with_local_defaults())?;
		let container_id = self.get_id()?;
		let res = docker.stop_container(&container_id, None).await;
		if let Err(bollard::errors::Error::DockerResponseServerError {
			status_code,
			..
		}) = res
		{
			// https://docs.docker.com/engine/api/v1.41/#tag/Container/operation/ContainerStop
			if status_code == 304
			{
				// container already stopped
				return Ok(false);
			}
		}
		report!(res)?;
		Ok(true)
	}

	/// container must be already stopped, otherwise this will fail
	/// returns stdout as UTF-8 lossy string
	pub async fn get_stdout(&self) -> Result<impl Stream<Item = String>>
	{
		if self.get_status().await? != ContainerStateStatusEnum::EXITED
		{
			return report!(Err(anyhow!(
				"Container {} is not stopped!",
				self.get_name()?
			)));
		}

		let docker = report!(Docker::connect_with_local_defaults())?;
		let container_id = self.get_id()?;
		let options = LogsOptions {
			// if follow is not false, the stream will kept alive
			// we don't want this so that we can cast the stream into a string
			follow: false,
			stdout: true,
			stderr: false,
			since: 0,
			// we want to get all logs, bug docker daemon seems to have a bug
			// that doesn't output any logs if `until` is too large
			until: i64::MAX / 2,
			timestamps: false,
			tail: "all"
		};

		let stream = docker.logs(&container_id, Some(options));
		let stream = stream.filter_map(|res| {
			res.ok().and_then(|log| {
				if let LogOutput::StdOut { message } = log
				{
					Some(String::from_utf8_lossy(&message).trim().to_string())
				}
				else
				{
					None
				}
			})
		});

		Ok(stream)
	}

	/// container must be already stopped, otherwise this will fail
	/// returns stderr as UTF-8 lossy string
	pub async fn get_stderr(&self) -> Result<impl Stream<Item = String>>
	{
		if self.get_status().await? != ContainerStateStatusEnum::EXITED
		{
			return report!(Err(anyhow!(
				"Container {} is not stopped!",
				self.get_name()?
			)));
		}

		let docker = report!(Docker::connect_with_local_defaults())?;
		let container_id = self.get_id()?;
		let options = LogsOptions {
			// if follow is not false, the stream will kept alive
			// we don't want this so that we can cast the stream into a string
			follow: false,
			stdout: false,
			stderr: true,
			since: 0,
			// we want to get all logs, bug docker daemon seems to have a bug
			// that doesn't output any logs if `until` is too large
			until: i64::MAX / 2,
			timestamps: false,
			tail: "all"
		};

		let stream = docker.logs(&container_id, Some(options));
		let stream = stream.filter_map(|res| {
			res.ok().and_then(|log| {
				if let LogOutput::StdErr { message } = log
				{
					Some(String::from_utf8_lossy(&message).trim().to_string())
				}
				else
				{
					None
				}
			})
		});

		Ok(stream)
	}

	/// container must be already stopped, otherwise this will fail
	/// removes the container
	/// takes ownership of self, so the container can't be used anymore
	pub async fn teardown(self) -> Result<()>
	{
		if self.get_status().await? != ContainerStateStatusEnum::EXITED
		{
			return report!(Err(anyhow!(
				"Container {} is not stopped!",
				self.get_name()?
			)));
		}

		let docker = report!(Docker::connect_with_local_defaults())?;
		let container_id = self.get_id()?;
		report!(docker.remove_container(&container_id, None).await)?;
		Ok(())
	}
}

mod tests
{
	// use super::*;

	#[ignore]
	#[tokio::test]
	async fn test_create()
	{
		todo!();
	}
}
