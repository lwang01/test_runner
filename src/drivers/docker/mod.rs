pub mod container;
pub mod network;

// Thin wrapper around bollard::Docker for ergonomics
// contains stateless operations only (i.e. not tied to a specific container)

use std::path::Path;

use bollard::{
	container::ListContainersOptions,
	models::{BuildInfo, ContainerSummary, ImageSummary},
	Docker
};
use tokio::fs;
use tokio_stream::StreamExt;
use tokio_util::codec;

use crate::{report, utils::Result};

pub struct DockerDaemon {}

impl DockerDaemon
{
	pub async fn list_containers(
		show_stopped: bool
	) -> Result<Vec<ContainerSummary>>
	{
		let docker = report!(Docker::connect_with_local_defaults())?;

		let options =
			ListContainersOptions { all: show_stopped, ..Default::default() };
		let containers =
			report!(docker.list_containers::<&str>(Some(options)).await)?;
		Ok(containers)
	}

	pub async fn list_images() -> Result<Vec<ImageSummary>>
	{
		let docker = report!(Docker::connect_with_local_defaults())?;
		let images = report!(docker.list_images::<&str>(None).await)?;
		Ok(images)
	}

	/// returns first image with matching tag
	/// tag is full tag including the colon, i.e. "ubuntu:latest"
	pub async fn image_with_tag_exists(
		tag: &str
	) -> Result<Option<ImageSummary>>
	{
		let images = DockerDaemon::list_images().await?;
		for image in images
		{
			for repo_tag in &image.repo_tags
			{
				if repo_tag == tag
				{
					return Ok(Some(image));
				}
			}
		}
		Ok(None)
	}

	/// loads image
	/// returns a Vec since it's apparently possible to load multiple images from a single file
	pub async fn load_images(path: &Path) -> Result<Vec<BuildInfo>>
	{
		let docker = report!(Docker::connect_with_local_defaults())?;

		// docker images can be large (~GiBs), so we stream as we read, rather than loading the entire image into memory
		let file = report!(fs::File::open(path).await)?;
		let stream = codec::FramedRead::new(file, codec::BytesCodec::new())
			.map(|r| r.map(|b| b.freeze()));

		// seems like single import call can end up importing more than 1 images
		let import_results = docker
			.import_image(
				Default::default(),
				hyper::Body::wrap_stream(stream),
				None
			)
			.collect::<Vec<_>>()
			.await;
		let mut imported = Vec::new();
		for import_result in import_results
		{
			let import = report!(import_result)?;
			imported.push(import);
		}

		Ok(imported)
	}
}

#[allow(clippy::unwrap_used)]
#[cfg(test)]
mod tests
{
	use super::*;

	#[ignore]
	#[tokio::test]
	async fn test_list_containers()
	{
		let containers = DockerDaemon::list_containers(true).await.unwrap();
		dbg!(containers);
	}

	#[ignore]
	#[tokio::test]
	async fn test_list_images()
	{
		let images = DockerDaemon::list_images().await.unwrap();
		dbg!(images);
	}
}
