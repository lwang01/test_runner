use std::{path::Path, time::Duration};

use anyhow::anyhow;
use bollard::{
	container::{Config, CreateContainerOptions, LogOutput, LogsOptions},
	models::{HostConfig, RestartPolicy, RestartPolicyNameEnum},
	service::ContainerStateStatusEnum,
	Docker
};
use tokio_stream::StreamExt;

use super::{super::container::Container, Network};
use crate::{
	drivers::docker::container::helper::ContainerExt,
	report,
	utils::Result
};

pub struct Sniffer
{
	dummy: Container,
	tcpdump: Container
}

impl Sniffer
{
	pub async fn teardown(mut self) -> Result<()>
	{
		report!(self.tcpdump.sigint().await)?;
		let stopped = report!(
			self.tcpdump
				.poll_for_status_with_timeout(
					ContainerStateStatusEnum::EXITED,
					Duration::from_secs(5)
				)
				.await
		)?;
		if !stopped
		{
			tracing::error!("tcpdump hung after receiving SIGINT, output cannot be trusted - MUST be re-ran");
			report!(self.tcpdump.stop().await)?;
		}
		let lines = report!(self.tcpdump.get_stderr().await)?;
		// tcpdump container is not expected to have long output, so aggregating it into a single string is fine
		for line in lines.collect::<Vec<_>>().await
		{
			let line = line.trim();
			if line.contains("packets dropped by kernel")
			{
				let num = report!(line
					.split(' ')
					.next()
					.ok_or_else(|| anyhow!("invalid tcpdump output")))?;
				let num = report!(num
					.parse::<u32>()
					.map_err(|_| anyhow!("invalid tcpdump output")))?;
				if num > 0
				{
					tracing::error!("{} dropped {} packets, output cannot be trusted - MUST be re-ran\nIf this happens frequently, increase -B flag of tcpdump command", self.tcpdump.get_name()?, num);
				}
			}
		}
		report!(self.tcpdump.teardown().await)?;
		report!(self.dummy.stop().await)?;
		report!(self.dummy.teardown().await)?;
		Ok(())
	}
}

#[async_trait::async_trait]
pub trait NetworkExt
{
	async fn start_tcpdump(
		&self, output_dir: &Path, filename: &str
	) -> Result<Sniffer>;
}

#[async_trait::async_trait]
impl NetworkExt for Network
{
	// we don't use `Container::create` abstraction, because we do some wack things with HostConfig here

	/// will output ${network_name}.pcap to to `output_dir`
	async fn start_tcpdump(
		&self, output_dir: &Path, filename: &str
	) -> Result<Sniffer>
	{
		let docker = report!(Docker::connect_with_local_defaults())?;

		// network interface doesn't come into life until any container using that network is started
		// so we create a dummy container that sleeps infinitely
		// this ensures tcpdump will be running before any containers that matter starts
		// There are other sleep containers, but I wanted to use an official docker image to be safe
		// this dummy container doubles as a container to list network namespaces, since we can't read these from host on macOS
		let dummy_name = format!("{}-dummy", self.get_name()?);
		let dummy_options = CreateContainerOptions { name: &dummy_name };
		let dummy_hostconfig = HostConfig {
			init: Some(true),
			// why this works on macOS:
			// Docker requires a host with a Linux kernel
			// because macOS != Linux, Docker for Mac runs on a Linux VM which sits on top of macOS's hypervisor
			// the `/var/run/docker/netns` exists inside this Linux VM, but because of macOS's hypervisor security model, it is not accessible from the macOS side
			// however, `docker` command on macOS transparently passes everything to the actual `docker` binary inside the Linux VM
			// so we can bind mount that directory to the dummy container, and access it from the dummy container!
			binds: Some(vec!["/var/run/docker/netns:/netns:ro".to_owned()]), /* TESTME: ro may break things? */
			network_mode: Some(self.get_name()?),
			restart_policy: Some(RestartPolicy {
				name: Some(RestartPolicyNameEnum::ALWAYS),
				..Default::default()
			}),
			auto_remove: Some(false),
			..Default::default()
		};
		let dummy_config = Config {
			image: Some("alpine:latest"),
			// list netns entry in each line and sleep infinitely
			entrypoint: Some(vec![
				"/bin/sh",
				"-c",
				"ls -1 /netns && sleep infinity",
			]),
			host_config: Some(dummy_hostconfig),
			..Default::default()
		};
		let dummy_res = report!(
			docker.create_container(Some(dummy_options), dummy_config).await
		)?;
		if !dummy_res.warnings.is_empty()
		{
			tracing::warn!("A warning has occured while creating dummy container for network {}: {}", &dummy_name, dummy_res.warnings.join("\n\n"));
		}
		let mut dummy = unsafe {
			Container::from_unchecked(report!(
				docker.inspect_container(&dummy_res.id, None).await
			)?)
		};
		dummy.start().await?;
		// read network namespaces
		// TESTME: will this be instant? there's no reliable way to poll, so if this doesn't work we'll have to sleep(1) or something...
		let dummy_logsoptions = LogsOptions {
			follow: false,
			stdout: true,
			stderr: false,
			since: 0,
			until: i64::MAX / 2,
			timestamps: false,
			tail: "all"
		};
		let mut network_namespace_ids = docker
			.logs(&dummy.get_id()?, Some(dummy_logsoptions))
			.collect::<Vec<_>>()
			.await
			.into_iter()
			.filter_map(|res| res.ok())
			.filter_map(|line| {
				let line = if let LogOutput::StdOut { message } = line
				{
					String::from_utf8_lossy(&message).trim().to_string()
				}
				else
				{
					return None;
				};
				if line.is_empty()
				{
					return None;
				}
				Some(line)
			});
		let network_id = self.get_id()?;
		// find netns for this network
		let netns = report!(network_namespace_ids
			.find(|entry| {
				// all swarm netns seems to start with `1-`, but don't want to rely on that
				// `.split()` is guaranteed to return at least 1 element
				let shorthash = if let Some((_, hash)) = entry.split_once('-')
				{
					hash
				}
				else
				{
					entry
				};
				network_id[..].starts_with(shorthash)
			})
			.ok_or_else(|| anyhow!(
				"Failed to find network namespace for network {}",
				&network_id
			)))?;

		// this is our actual tcpdump container
		// it runs in privileged mode on the host, and `nsenter` into docker network namespace
		let container_name = format!("{}-tcpdump", self.get_name()?);
		let options = CreateContainerOptions { name: &container_name };

		let host_config = HostConfig {
			// we want to send SIGINT to tcpdump to get drop counts
			init: Some(true),
			binds: Some(vec![
				// place to store our pcap file
				format!(
					"{}:/root:rw",
					report!(output_dir.canonicalize())?.display(),
				),
				// bind mount docker virtual bridge
				"/var/run/docker/netns:/var/run/docker/netns".to_owned(),
			]),
			// necessary for nsenter
			privileged: Some(true),
			restart_policy: Some(RestartPolicy {
				name: Some(RestartPolicyNameEnum::NO),
				..Default::default()
			}),
			auto_remove: Some(false),
			..Default::default()
		};
		let config = Config {
			// FIXME: pinning untrusted container to latest is potentially dangerous
			image: Some("nicolaka/netshoot".to_owned()),
			working_dir: Some("/root".to_owned()),
			entrypoint: Some(vec![
				"nsenter".to_owned(),
				format!("--net=/var/run/docker/netns/{}", netns),
				"sh".to_owned(),
				"-c".to_owned(),
				// -n option must be present to prevent tcpdump from resolving hostnames
				// since our virtual network is isolated from the Internet, resolving will fail and cause 10s lag
				// --immediate-mode disables packet buffering and -l disables output buffering
				format!(
					"tcpdump -ln -B 262144 -i br0 -w {}.pcap --immediate-mode",
					filename
				),
			]),
			host_config: Some(host_config),
			..Default::default()
		};
		let res =
			report!(docker.create_container(Some(options), config).await)?;

		if !res.warnings.is_empty()
		{
			tracing::warn!(
				"A warning has occurred while creating a docker container {}: {}",
				container_name,
				res.warnings.join("\n\n")
			);
		}

		let container = report!(docker.inspect_container(&res.id, None).await)?;
		let mut tcpdump = unsafe { Container::from_unchecked(container) };
		tcpdump.start().await?;

		Ok(Sniffer { dummy, tcpdump })
	}
}
