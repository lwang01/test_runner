use std::time::Duration;

use anyhow::anyhow;
use bollard::{
	models::Network as BNetwork,
	network::CreateNetworkOptions,
	Docker
};

use crate::{report, utils::Result};

pub mod helper;

pub struct Network
{
	// for internal tracking of container id only
	// not to be used as a cache / direct access!
	network: BNetwork
}

impl Network
{
	/// create a new docker network with the given name
	/// if internet is true, the network will be connected to the internet
	pub async fn create(name: &str, internet: bool) -> Result<Network>
	{
		let docker = report!(Docker::connect_with_local_defaults())?;

		let options = CreateNetworkOptions {
			name,
			check_duplicate: true,
			// "normal" docker networks in bridge mode are switched by untappable bridge, so we can't sniff "everything on the net"
			// overlay networks has a bridge that is tappable, allowing us to reliably sniff everything on the network
			// this requires docker to be in swarm mode, though
			driver: "overlay",
			attachable: true,
			internal: !internet,
			..Default::default()
		};
		let res = report!(docker.create_network(options).await)?;
		if let Some(w) = res.warning
		{
			if !w.trim().is_empty()
			{
				tracing::warn!(
					"A warning has occurred while creating a docker network {}: {}",
					name,
					w
				);
			}
		}
		let network =
			report!(docker.inspect_network::<&str>(name, None).await)?;
		Ok(Network { network })
	}
}

impl Network
{
	pub fn get_name(&self) -> Result<String>
	{
		report!(self
			.network
			.name
			.clone()
			.ok_or_else(|| anyhow!("Network has no name: {:?}", &self.network)))
	}

	pub fn get_id(&self) -> Result<String>
	{
		report!(self
			.network
			.id
			.clone()
			.ok_or_else(|| anyhow!("Network has no id: {:?}", &self.network)))
	}

	pub async fn inspect(&self) -> Result<BNetwork>
	{
		let docker = report!(Docker::connect_with_local_defaults())?;
		let network_name = self.get_name()?;
		report!(docker.inspect_network::<&str>(&network_name, None).await)
	}

	pub async fn teardown(self) -> Result<()>
	{
		let docker = report!(Docker::connect_with_local_defaults())?;
		let name = self.get_name()?;
		let mut i = 0;
		let err = loop
		{
			let res = docker.remove_network(&name).await;
			if let Err(e) = res
			{
				if let bollard::errors::Error::DockerResponseServerError {
					status_code,
					..
				} = e
				{
					if status_code == 400
					{
						// all containers are removed, but network still thinks it's there
						// retry a few times
						if i > 5
						{
							break e;
						}
						i += 1;
						// linear backoff
						tokio::time::sleep(Duration::from_secs(i)).await;
						continue;
					}
					else
					{
						break e;
					}
				}
				else
				{
					break e;
				}
			}
			else
			{
				return Ok(());
			}
		};
		report!(Err(err))
	}
}
