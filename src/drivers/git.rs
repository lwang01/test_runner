use std::{
	path::{Path, PathBuf},
	process::Stdio
};

use anyhow::anyhow;
use chrono::{DateTime, Utc};
use tokio::process::Command;

use crate::{report, utils::Result};

pub struct GitRepo
{
	directory: PathBuf
}

impl GitRepo
{
	/// check if current directory is a git repository
	pub async fn is_git_repo(directory: &Path) -> Result<bool>
	{
		let directory = report!(directory.canonicalize())?;

		let output = report!(
			Command::new("git")
				.current_dir(&directory)
				.arg("rev-parse")
				.arg("--show-toplevel")
				.stdout(Stdio::null())
				.stderr(Stdio::null())
				.output()
				.await
		)?;

		if !output.status.success()
		{
			return Ok(false);
		}

		let repo_root = String::from_utf8_lossy(&output.stdout);
		let repo_root = Path::new(repo_root.trim());

		Ok(repo_root == directory)
	}

	pub async fn open(directory: &Path) -> Result<Self>
	{
		let directory = report!(directory.canonicalize())?;

		if !GitRepo::is_git_repo(&directory).await?
		{
			return report!(Err(anyhow!(
				"{} is not a git repository",
				directory.display()
			)));
		}

		let git = Self { directory };

		Ok(git)
	}

	pub async fn clone(url: &str, parent: &Path, name: &str) -> Result<Self>
	{
		let parent = report!(parent.canonicalize())?;
		let directory = parent.join(name);
		if directory.is_dir()
		{
			return report!(Err(anyhow!(
				"{} already exists",
				directory.display()
			)));
		}

		let output = report!(
			Command::new("git")
				.current_dir(&parent)
				.arg("clone")
				.arg(url)
				.arg(&name)
				.stdout(Stdio::null())
				.stderr(Stdio::null())
				.output()
				.await
		)?;

		if !output.status.success()
		{
			return report!(Err(anyhow!("git clone failed: {:?}", output)));
		}

		Self::open(&directory).await
	}
}

impl GitRepo
{
	/// get path to repository
	pub fn get_path(&self) -> &Path
	{
		&self.directory
	}

	/// git fetch --all
	pub async fn fetch_all(&self) -> Result<()>
	{
		let output = report!(
			Command::new("git")
				.current_dir(&self.directory)
				.arg("fetch")
				.arg("--all")
				.stdout(Stdio::null())
				.stderr(Stdio::null())
				.output()
				.await
		)?;

		if !output.status.success()
		{
			return report!(Err(anyhow!("git fetch failed: {:?}", output)));
		}

		Ok(())
	}

	/// git reset --hard to provided hash
	pub async fn reset_to_hash(&self, hash: &str) -> Result<()>
	{
		let output = report!(
			Command::new("git")
				.current_dir(&self.directory)
				.arg("reset")
				.arg("--hard")
				.arg(hash)
				.stdout(Stdio::null())
				.stderr(Stdio::null())
				.output()
				.await
		)?;

		if !output.status.success()
		{
			return report!(Err(anyhow!("git reset failed: {:?}", output)));
		}

		Ok(())
	}

	/// git rev-parse HEAD
	/// returns the hash of the current commit
	pub async fn get_current_hash(&self) -> Result<String>
	{
		let output = report!(
			Command::new("git")
				.current_dir(&self.directory)
				.arg("rev-parse")
				.arg("HEAD")
				.stdout(Stdio::null())
				.stderr(Stdio::null())
				.output()
				.await
		)?;

		if !output.status.success()
		{
			return report!(Err(anyhow!("git rev-parse failed: {:?}", output)));
		}

		let hash = report!(String::from_utf8(output.stdout))?;
		let hash = hash.trim();

		Ok(hash.to_string())
	}

	/// git show --no-patch --no-notes --pretty='%cd' --date=iso-strict <hash>
	/// returns parsed datetime of commit
	pub async fn get_hash_datetime(&self, hash: &str) -> Result<DateTime<Utc>>
	{
		let output = report!(
			Command::new("git")
				.current_dir(&self.directory)
				.arg("show")
				.arg("--no-patch")
				.arg("--no-notes")
				.arg("--pretty=%cd")
				.arg("--date=iso-strict")
				.arg(hash)
				.stdout(Stdio::null())
				.stderr(Stdio::null())
				.output()
				.await
		)?;

		if !output.status.success()
		{
			return report!(Err(anyhow!("git show failed: {:?}", output)));
		}

		let datetime = String::from_utf8_lossy(&output.stdout);
		let datetime = report!(DateTime::parse_from_rfc3339(datetime.trim()))?;
		let datetime = datetime.with_timezone(&Utc);

		Ok(datetime)
	}
}

#[cfg(test)]
mod tests
{
	use super::*;

	#[tokio::test]
	async fn test_is_git_repo() -> Result<()>
	{
		assert!(GitRepo::is_git_repo(Path::new(".")).await?);

		assert!(!GitRepo::is_git_repo(Path::new("src")).await?);

		Ok(())
	}

	#[tokio::test]
	async fn test_get_hash_datetime() -> Result<()>
	{
		let git = GitRepo::open(Path::new(".")).await?;
		let datetime = git
			.get_hash_datetime("3070fbc7174217cd98f18c229c60ed503f0f6f77")
			.await?;
		assert!(datetime.timestamp() > 0);

		Ok(())
	}

	#[ignore]
	#[tokio::test]
	async fn test_git_clone() -> Result<()>
	{
		let git = GitRepo::clone(
			"https://github.com/octocat/Hello-World.git",
			Path::new("."),
			"_ignore_test_delme_repo"
		)
		.await?;

		Ok(())
	}
}
