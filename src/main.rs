#![warn(clippy::unwrap_used)]
#![warn(clippy::expect_used)]

use std::{env, path::Path, sync::Arc, time::Duration};

use bollard::{service::ContainerStateStatusEnum, Docker};
pub use chrono_tz::America::New_York as TZ; /* TZ to use for assignment due */
use clap::Parser;
use futures::StreamExt;
use sysinfo::{RefreshKind, System, SystemExt};
use tokio::{fs, sync::Semaphore};
use tracing::*;

mod args;
mod drivers;
mod gitlab;
mod repo;
mod types;
mod utils;
mod yaml;

use crate::{
	drivers::docker::{
		container::{
			helper::{ContainerExt, DockerExt},
			Container
		},
		DockerDaemon
	},
	gitlab::{helper::GitLabExt, GitLab},
	repo::{GradingAggregator, GradingArgs, StudentRepo},
	utils::{DateTimeUtcExt, ToVecStrExt}
};

/// macro that sleeps 5s if c is true
macro_rules! confirm {
	($c:expr) => {
		if $c
		{
			warn!("Is this OK? Press Ctrl+C in 3 seconds to abort!");
			tokio::time::sleep(std::time::Duration::from_secs(3)).await;
		}
	};
}

#[allow(clippy::expect_used)]
#[tokio::main]
async fn main()
{
	// async debugger init
	// see https://github.com/tokio-rs/console for usage
	// should be commented out for normal usage
	// console_subscriber::init();

	// parse args
	let args = args::Args::parse();

	// initialize logger
	if env::var("RUST_LOG").is_err()
	{
		env::set_var("RUST_LOG", format!("{}=info", env!("CARGO_PKG_NAME")));
	}
	tracing_subscriber::fmt::init();

	// overlay networks require docker daemon to be in swarm mode
	let is_swarm = Docker::is_swarm_mode()
		.await
		.expect("Failed to get swarm mode status from Docker daemon!");
	if !is_swarm
	{
		error!("Docker daemon is not running on swarm mode.");
		error!("This is necessary for reliable packet capturing on virtual networks.");
		error!("Please run `docker swarm init` and try again.");
		std::process::exit(1);
	}

	// parse args into usable types
	let parsed_args =
		args::ParsedArgs::new(args).await.expect("args sancheck failed");
	let c = !parsed_args.no_confirm;

	// check if docker image exists, import if not
	// path is already san-checked in args::ParsedArgs::new
	let docker_image_path = Path::new(&parsed_args.config.docker_image_path);
	if DockerDaemon::image_with_tag_exists(&parsed_args.config.docker_image_tag)
		.await
		.expect("failed to check if docker image exists")
		.is_none()
	{
		info!(
			"Docker image {} does not exist, importing...",
			&parsed_args.config.docker_image_tag
		);
		let images = DockerDaemon::load_images(docker_image_path)
			.await
			.expect("Failed to import image");
		info!("Imported {} image(s)", images.len());
	}

	info!(
		"Connecting to GitLab instance on {}",
		parsed_args.config.gitlab_base_url
	);
	info!("Your GitLab PAT will be sent to above url.");
	confirm!(c);

	// log in to gitlab
	let gitlab = GitLab::new(
		parsed_args.config.gitlab_base_url.clone(),
		&parsed_args.credentials.gitlab_token
	)
	.await
	.expect("failed to create gitlab client");
	let gl_user =
		gitlab.get_current_user().await.expect("failed to get current user");

	info!("Connected to GitLab as {}", gl_user.username);
	info!(
		"Using directory {} to clone student repos",
		parsed_args.workdir.display()
	);
	info!("{} students found", parsed_args.students.len());
	confirm!(c);

	info!("Fetching meta from GitLab... (this may take a while)");
	let gl_groups = gitlab
		.get_subgroups(parsed_args.config.gitlab_group_id)
		.await
		.expect("failed to get groups");
	let mut repos = Vec::new();

	// fetch info from gitlab and clone for each student (sequential)
	for (student_id, extension) in parsed_args.students
	{
		// get gitlab group corresponding to student
		let s_group = gl_groups
			.iter()
			.find(|g| g.name == student_id)
			.unwrap_or_else(|| panic!("failed to find group {}", student_id));

		// get the project from said group
		let gl_proj = gitlab
			.get_project(
				&parsed_args.assignment.config.repo_name,
				s_group.id.value()
			)
			.await
			.unwrap_or_else(|_| {
				panic!(
					"failed to find project {} in group {}",
					&parsed_args.assignment.config.repo_name, student_id
				)
			});

		// calculate due with extension
		let due = parsed_args
			.assignment
			.config
			.due
			.with_extension(extension)
			.expect("Failed to calculate due date");

		// get commit hash before due
		let hash = gitlab
			.get_commit_hash_before_deadline(gl_proj.id.value(), due)
			.await
			.unwrap_or_else(|_| {
				panic!(
					"failed to get commit hash for project {} in group {}",
					&gl_proj.name, &s_group.name
				)
			});

		// create debug workdir
		let debug_workdir = parsed_args.debug_workdir.join(student_id.clone());
		if !debug_workdir.is_dir()
		{
			fs::create_dir(&debug_workdir)
				.await
				.expect("failed to create debug workdir");
		}

		// create repo
		let repo = StudentRepo::new(
			student_id,
			hash,
			gl_proj.ssh_url_to_repo,
			due,
			&parsed_args.workdir,
			parsed_args.assignment.clone(),
			parsed_args.config.docker_image_tag.clone(),
			debug_workdir,
			parsed_args.assignment.reference_code_path.clone()
		)
		.await
		.expect("failed to create student repo");
		repos.push(repo);
	}

	info!("Building reference code...");
	// build reference repo
	if !parsed_args.assignment.config.no_reference
	{
		let container_name = format!(
			"reference-{}-builder",
			parsed_args.assignment.config.repo_name
		);
		let mut container = Container::create(
			&container_name,
			&parsed_args.config.docker_image_tag,
			&parsed_args.assignment.reference_code_path,
			false,
			vec![],
			parsed_args.assignment.config.build_command.to_vec_str(),
			None
		)
		.await
		.expect("Failed to create builder for reference code");
		container
			.start()
			.await
			.expect("Failed to start builder for reference code");
		let success = container
			.poll_for_status_with_timeout(
				ContainerStateStatusEnum::EXITED,
				Duration::from_secs(120)
			)
			.await
			.expect("Failed to poll for status of builder for reference code");
		if !success
		{
			container
				.stop()
				.await
				.expect("Failed to stop builder for reference code");
			panic!("Failed to build reference code");
		}
		// ref code build output is "trusted" to not be too large, so aggregating is fine
		let stdout = container
			.get_stdout()
			.await
			.expect("Failed to get stdout of builder for reference code")
			.collect::<Vec<_>>()
			.await;
		let stderr = container
			.get_stderr()
			.await
			.expect("Failed to get stderr of builder for reference code")
			.collect::<Vec<_>>()
			.await;
		info!("Reference code build stdout:");
		for line in stdout
		{
			println!("{}", line);
		}
		info!("Reference code build stderr:");
		for line in stderr
		{
			println!("{}", line);
		}
		container
			.teardown()
			.await
			.expect("Failed to teardown builder for reference code");
	}

	// multithreaded grading
	let concurrency = num_cpus::get_physical();
	let concurrency = if let Some(concurrency_limit) =
		parsed_args.max_concurrent
	{
		std::cmp::min(concurrency, concurrency_limit)
	}
	else
	{
		concurrency
	};
	// use physical CPU core count or Available RAM / 1024MiB, whichever is smaller
	// if less than 1GB of free ram, use 1 thread
	let sysinfo = System::new_with_specifics(RefreshKind::new().with_memory());
	let avail_ram = sysinfo.available_memory() as usize;
	let avail_ram_1024mib = avail_ram / (1024 * 1024 * 1024);
	let concurrency = if concurrency > avail_ram_1024mib
	{
		warn!("Running low on memory, using less threads than physical CPU to ensure each container gets at least 1GiB of RAM");
		warn!("Available RAM reported: {} MiB", avail_ram / 1024 / 1024);
		if avail_ram_1024mib < 1
		{
			1
		}
		else
		{
			avail_ram_1024mib
		}
	}
	else
	{
		concurrency
	};
	info!("Running in {} threads...", concurrency);
	// semaphore to limit concurrency
	let semaphore = Arc::new(Semaphore::new(concurrency));
	let mut join_handles = Vec::new();

	// for each student repos
	for repo in repos
	{
		// block loop if concurrency limit reached
		let permit = semaphore
			.clone()
			.acquire_owned()
			.await
			.expect("Semaphore was corrupted!");

		// spawn a new thread
		let ta_username = gl_user.username.clone();
		let handle = tokio::spawn(async move {
			let (hash, commit_date) = if parsed_args.no_reset
			{
				repo.get_current_hash_dt()
					.await
					.expect("failed to get current hash")
			}
			else
			{
				repo.reset().await.expect("failed to reset repo")
			};
			info!(
				"Using {} for {}\t({})",
				&hash[..7],
				repo.directory_id,
				commit_date.to_loc_string()
			);

			// clear old test results
			repo.clear_debug().await.expect("failed to clear debug");

			// write metadata
			repo.write_meta(&ta_username)
				.await
				.expect("failed to write metadata");

			// build
			repo.build().await.expect("failed to build student repo");

			let build_stderr = repo
				.read_debug("build.stderr")
				.await
				.expect("failed to read build stdout");
			if build_stderr
				.contains_substr("No targets specified and no makefile found")
			{
				warn!("{} has no makefile, skipping", repo.directory_id);
				drop(permit);
				return;
			}

			// run test
			if let Err(e) = repo.run_tests().await
			{
				error!(
					"failed to test {}, please do a re-run for this student",
					repo.directory_id
				);
				println!("DEBUG INFO:");
				println!("{:?}", e);
			}
			// grade
			let grading_args = GradingArgs {
				student_tests_output_dir: repo.debug_dir.clone(),
				student_repo_dir: repo.git.get_path().to_path_buf(),
				score_output_name: Some("scores.out".to_string()), /* Produces delimited output for TA's of scores and pass/fail of tests */
				comments_output_name: Some("grades.txt".to_string()), /* Student-facing grading comments with score */
				no_overwrite: None,
				additional_args: None
			};
			if let Err(e) = repo.grade(&grading_args).await
			{
				error!("failed to grade {}", repo.directory_id);
				println!("DEBUG INFO:");
				println!("{:?}", e);
			}

			drop(permit);
		});
		join_handles.push(handle);
	}

	let mut errors = 0;
	for handle in join_handles
	{
		if handle.await.is_err()
		{
			errors += 1;
		}
	}

	if errors == 0
	{
		info!("All graders finished. Check above logs for any errors.");
	}
	else
	{
		error!("{} grader(s) crashed. See the logs for panic messages and re-run the grader for affected students!", errors);
	}

	// Run the grading aggregator when finished
	let aggregated_output_filename = Some(format!("{}.out", parsed_args.assignment.config.repo_name));
	let grading_aggregator = GradingAggregator {
		assignment_output_dir: parsed_args.debug_workdir.clone(),
		aggregated_output_dir: parsed_args.debug_workdir.clone(),
		aggregated_output_filename,
		student_score_filename: Some("scores.out".to_string()),
		no_overwrite: None,
		no_student_info: None,
	};

	if let Err(e) = grading_aggregator.aggregate(&parsed_args.assignment.grader_path).await
	{
		error!("failed to aggregate grades");
		println!("DEBUG INFO:");
		println!("{:?}", e);
	}

}
