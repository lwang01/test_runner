// typedefs and deserializers for ALL yaml files used for this project

use chrono::{DateTime, Datelike, NaiveDateTime, TimeZone, Timelike, Utc};
use linked_hash_map::LinkedHashMap;
use serde::{Deserialize, Deserializer, Serialize};
use url::Url;

use crate::TZ;

#[derive(Debug, Serialize)]
pub struct RepositoryMeta
{
	#[serde(rename = "Commit Hash")]
	pub commit_hash: String,
	#[serde(rename = "Due")]
	pub due: String,
	#[serde(rename = "TA")]
	pub ta: String
}

// credentials yaml
#[derive(Debug, Deserialize)]
pub struct Credentials
{
	pub gitlab_token: String
}

// config yaml
#[derive(Deserialize, Debug)]
pub struct Config
{
	pub gitlab_base_url: Url,
	pub gitlab_group_id: u64,
	pub docker_image_tag: String,
	pub docker_image_path: String
}

// assignment yaml
#[derive(Deserialize, Debug, Clone)]
pub struct AssignmentConfig
{
	pub repo_name: String,
	#[serde(deserialize_with = "from_monkey_timestring")]
	pub due: DateTime<Utc>,
	pub no_reference: bool,
	pub build_command: Vec<String>
}

// container yaml
// we use linked hash map to preserve order of containers
#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "lowercase")]
pub struct ContainerConfig
{
	#[serde(default = "default_false")]
	pub tcpdump: bool,
	pub simultaneous_launch: bool,
	pub expected_artifacts: Vec<String>,
	pub layout: LinkedHashMap<String, ContainerLayout>
}

// container yaml
#[derive(Deserialize, Debug, Clone)]
pub struct ContainerLayout
{
	pub reference: bool,
	pub read_only: bool,
	pub launch_cmd: Vec<String>,
	pub timeout: i64,
	pub additional_mounts: Vec<(String, String, bool)>
}

// I strongly dislike DST
// NOTE: if congress does end up abolishing DST (which they should), update the timezone definition crate by running:
// cargo install cargo-edit && cargo upgrade
fn from_monkey_timestring<'de, D>(
	deserializer: D
) -> Result<DateTime<Utc>, D::Error>
where
	D: Deserializer<'de>
{
	let s: &str = Deserialize::deserialize(deserializer)?;
	let no_tz_parsed = NaiveDateTime::parse_from_str(s, "%Y-%m-%d %H:%M:%S")
		.map_err(serde::de::Error::custom)?;
	let dt = TZ
		.ymd(no_tz_parsed.year(), no_tz_parsed.month(), no_tz_parsed.day())
		.and_hms(
			no_tz_parsed.hour(),
			no_tz_parsed.minute(),
			no_tz_parsed.second()
		);
	Ok(dt.with_timezone(&Utc))
}

fn default_false() -> bool
{
	false
}

#[allow(clippy::unwrap_used)]
#[cfg(test)]
mod tests
{
	use chrono::NaiveDate;

	use super::*;

	#[derive(Deserialize, Debug)]
	struct Test
	{
		#[serde(deserialize_with = "from_monkey_timestring")]
		pub due: DateTime<Utc>
	}

	#[test]
	fn test_date_parse_dst()
	{
		let input = r#"
		{
			"due": "2022-09-13 23:59:59"
		}
		"#;
		let during_dst: Test = serde_json::from_str(input).unwrap();
		let expected_utc = NaiveDate::from_ymd(2022, 9, 14).and_hms(3, 59, 59);
		let expected: DateTime<Utc> = DateTime::from_utc(expected_utc, Utc);
		assert_eq!(during_dst.due, expected);
	}

	#[test]
	fn test_date_parse_standard()
	{
		let input = r#"
		{
			"due": "2023-02-12 23:59:59"
		}
		"#;
		let during_standard: Test = serde_json::from_str(input).unwrap();
		let expected_utc = NaiveDate::from_ymd(2023, 2, 13).and_hms(4, 59, 59);
		let expected: DateTime<Utc> = DateTime::from_utc(expected_utc, Utc);
		assert_eq!(during_standard.due, expected);
	}

	#[derive(Deserialize, Debug)]
	struct Test2
	{
		#[serde(default = "default_false")]
		pub tcpdump: bool
	}

	#[test]
	fn test_deserialize_or_false()
	{
		let input = r#"{}"#;
		let empty: Test2 = serde_json::from_str(input).unwrap();
		assert!(!empty.tcpdump);
	}
}
