// custom type definitions that don't really fit anywhere else

use std::{collections::BTreeMap, path::PathBuf};

use crate::yaml::{AssignmentConfig, ContainerConfig};

// holder type for assignment
#[derive(Debug, Clone)]
pub struct Assignment
{
	pub config: AssignmentConfig,
	pub reference_code_path: PathBuf,
	pub tests_path: PathBuf,
	pub grader_path: PathBuf,
	pub tests: BTreeMap<String, ContainerConfig>
}
