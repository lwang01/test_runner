use anyhow::anyhow;
use chrono::{DateTime, Utc};
use gitlab::types::{Group, Project, RepoCommit, UserBasic};

use crate::{report, utils::Result};

#[async_trait::async_trait]
pub trait GitLabExt
{
	async fn get_current_user(&self) -> Result<UserBasic>;
	async fn get_subgroups(&self, group_id: u64) -> Result<Vec<Group>>;
	async fn get_project(
		&self, project_name: &str, group_id: u64
	) -> Result<Project>;
	async fn get_commit_hash_before_deadline(
		&self, project_id: u64, deadline: DateTime<Utc>
	) -> Result<String>;
}

#[async_trait::async_trait]
impl GitLabExt for super::GitLab
{
	async fn get_current_user(&self) -> Result<UserBasic>
	{
		self.get::<UserBasic>("/api/v4/user").await
	}

	async fn get_subgroups(&self, group_id: u64) -> Result<Vec<Group>>
	{
		self.get::<Vec<Group>>(&format!(
			"/api/v4/groups/{}/subgroups",
			group_id
		))
		.await
	}

	async fn get_project(
		&self, project_name: &str, group_id: u64
	) -> Result<Project>
	{
		let mut projects = self
			.get::<Vec<Project>>(&format!(
				"/api/v4/groups/{}/projects?search={}",
				group_id, project_name
			))
			.await?;
		if projects.len() == 1
		{
			Ok(projects.remove(0))
		}
		else
		{
			report!(Err(anyhow!("Incorrect number of projects returned from API: expected 1, found {}", projects.len())))
		}
	}

	async fn get_commit_hash_before_deadline(
		&self, project_id: u64, deadline: DateTime<Utc>
	) -> Result<String>
	{
		let commits = self
			.get::<Vec<RepoCommit>>(&format!(
				"/api/v4/projects/{}/repository/commits",
				project_id
			))
			.await?;
		let commit =
			commits.into_iter().find(|commit| commit.committed_date < deadline);
		match commit
		{
			Some(commit) => Ok(commit.id.value().trim().to_owned()),
			None => report!(Err(anyhow!("No commit found before deadline")))
		}
	}
}
