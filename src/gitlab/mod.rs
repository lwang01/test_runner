// REST GitLab Client
// crate `gitlab` in cargo.toml is for typedefs ONLY!!!

use anyhow::anyhow;
use hyper::http::HeaderValue;
use reqwest::{header::HeaderMap, Client};
use serde::{Deserialize, Serialize};
use url::Url;

use crate::{report, utils::Result};

pub mod helper;

#[derive(Debug, Deserialize)]
pub struct Ignore {}

pub struct GitLab
{
	client: Client,
	base_url: Url
}

impl GitLab
{
	pub async fn new(base_url: Url, token: &str) -> Result<Self>
	{
		let mut headers = HeaderMap::new();
		headers.insert("PRIVATE-TOKEN", report!(token.parse::<HeaderValue>())?);
		let client = report!(Client::builder()
			.default_headers(headers)
			.gzip(true)
			.build())?;
		Ok(Self { client, base_url })
	}
}

impl GitLab
{
	pub async fn get<T: for<'de> Deserialize<'de>>(
		&self, endpoint: &str
	) -> Result<T>
	{
		let url = report!(self.base_url.join(endpoint))?;
		let res = report!(
			self.client
				.get(url.clone())
				.query(&[("per_page", "100")])
				.send()
				.await
		)?;
		if !res.status().is_success()
		{
			report!(Err(anyhow!(
				"URL: {}\nError: {}",
				url,
				report!(res.text().await)?
			)))
		}
		else
		{
			Ok(report!(res.json().await)?)
		}
	}

	pub async fn _post<T: for<'de> Deserialize<'de>, U: Serialize + ?Sized>(
		&self, endpoint: &str, body: &U
	) -> Result<T>
	{
		let url = report!(self.base_url.join(endpoint))?;
		let res =
			report!(self.client.post(url.clone()).json(body).send().await)?;
		if !res.status().is_success()
		{
			report!(Err(anyhow!(
				"URL: {}\nError: {}",
				url,
				report!(res.text().await)?
			)))
		}
		else
		{
			Ok(report!(res.json().await)?)
		}
	}

	pub async fn _put<T: for<'de> Deserialize<'de>, U: Serialize + ?Sized>(
		&self, endpoint: &str, body: &U
	) -> Result<T>
	{
		let url = report!(self.base_url.join(endpoint))?;
		let res =
			report!(self.client.put(url.clone()).json(body).send().await)?;
		if !res.status().is_success()
		{
			report!(Err(anyhow!(
				"URL: {}\nError: {}",
				url,
				report!(res.text().await)?
			)))
		}
		else
		{
			Ok(report!(res.json().await)?)
		}
	}
}
