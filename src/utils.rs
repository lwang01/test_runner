use std::{path::Path, time::Duration};

use anyhow::Error;
use chrono::{DateTime, Duration as CDuration, Utc};
use error_stack::Result as ESResult;

/// error_stack::Result<T, anyhow::Error>
pub type Result<T> = ESResult<T, Error>;

#[macro_export]
macro_rules! report {
	($e:expr) => {{
		use ::anyhow::anyhow;
		use ::error_stack::IntoReportCompat;
		$e.map_err(|e| anyhow!(e)).into_report()
	}};
}

pub trait DateTimeUtcExt
{
	fn with_extension(
		&self, extension: Option<Duration>
	) -> Result<DateTime<Utc>>;
	fn to_loc_string(&self) -> String;
}

pub trait ToVecStrExt
{
	fn to_vec_str(&self) -> Vec<&str>;
	fn contains_substr(&self, s: &str) -> bool;
}

pub trait PathExt
{
	fn canonicalize_to_str(&self) -> Result<String>;
}

impl DateTimeUtcExt for DateTime<Utc>
{
	fn with_extension(
		&self, extension: Option<Duration>
	) -> Result<DateTime<Utc>>
	{
		Ok(*self
			+ report!(CDuration::from_std(extension.unwrap_or_default()))?
			+ CDuration::minutes(1))
	}

	fn to_loc_string(&self) -> String
	{
		self.with_timezone(&crate::TZ).to_string()
	}
}

impl ToVecStrExt for Vec<String>
{
	fn to_vec_str(&self) -> Vec<&str>
	{
		self.iter().map(|s| &s[..]).collect()
	}

	fn contains_substr(&self, s: &str) -> bool
	{
		self.iter().any(|s2| s2.contains(s))
	}
}

impl PathExt for Path
{
	fn canonicalize_to_str(&self) -> Result<String>
	{
		match self.canonicalize()
		{
			Ok(path) => Ok(path.display().to_string()),
			Err(_) =>
			{
				report!(Err(anyhow!(
					"Path {} could not be canonicalized",
					self.display()
				)))
			}
		}
	}
}
