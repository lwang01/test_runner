// abstraction for single student repository
// contains all data necessary for grading
use std::path::{Path, PathBuf};

use chrono::{DateTime, Utc};
use error_stack::ResultExt;
use futures::{stream, Stream, StreamExt};
use tokio::{
	fs,
	io::{AsyncBufReadExt, AsyncWriteExt, BufReader, BufWriter}
};
use tokio_stream::wrappers::LinesStream;

use crate::{
	drivers::git::GitRepo,
	report,
	types::Assignment,
	utils::Result,
	yaml::RepositoryMeta
};

mod grade;

pub use self::grade::{GradingArgs, GradingAggregator};

pub struct StudentRepo
{
	pub directory_id: String,
	pub hash: String, // hash to grade (from gitlab)
	pub due: DateTime<Utc>,
	pub git: GitRepo,
	pub assignment: Assignment,
	pub image_tag: String,
	pub debug_dir: PathBuf,
	pub reference_path: PathBuf
}

impl StudentRepo
{
	// TODO: may need refactoring?
	#[allow(clippy::too_many_arguments)]
	pub async fn new(
		directory_id: String, hash: String, clone_url: String,
		due: DateTime<Utc>, parent: &Path, assignment: Assignment,
		image_tag: String, debug_dir: PathBuf, reference_path: PathBuf
	) -> Result<Self>
	{
		let repo_directory_path = parent.join(&directory_id);
		let git = if repo_directory_path.is_dir()
		{
			let git = GitRepo::open(&repo_directory_path).await?;
			git.fetch_all().await?;
			git
		}
		else
		{
			tracing::warn!(
				"{} does not exist, cloning",
				repo_directory_path.display()
			);
			GitRepo::clone(&clone_url, parent, &directory_id).await?
		};
		Ok(Self {
			directory_id,
			hash,
			due,
			git,
			assignment,
			image_tag,
			debug_dir,
			reference_path
		})
	}
}

impl StudentRepo
{
	/// reset repository to provided hash
	/// returns full hash the repository was reset to and its commit datetime
	pub async fn reset(&self) -> Result<(String, DateTime<Utc>)>
	{
		self.git.reset_to_hash(&self.hash).await?;
		let dt = self.git.get_hash_datetime(&self.hash).await?;
		if dt > self.due
		{
			tracing::warn!("{}'s repository was reset to {} which was committed after due {}", self.directory_id, self.hash, self.due);
		}
		Ok((self.git.get_current_hash().await?, dt))
	}

	/// gets current hash and its datetime
	pub async fn get_current_hash_dt(&self) -> Result<(String, DateTime<Utc>)>
	{
		let hash = self.git.get_current_hash().await?;
		let dt = self.git.get_hash_datetime(&hash).await?;
		Ok((hash, dt))
	}

	/// clear debug directory
	pub async fn clear_debug(&self) -> Result<()>
	{
		if self.debug_dir.is_dir()
		{
			report!(fs::remove_dir_all(&self.debug_dir).await)?;
		}
		report!(fs::create_dir_all(&self.debug_dir).await)?;
		Ok(())
	}

	/// writes lines to debug directory
	///
	/// if append is false, file will be overwritten
	pub async fn write_debug(
		&self, filename: &str,
		mut lines: impl Stream<Item = String> + std::marker::Unpin,
		append: bool
	) -> Result<()>
	{
		let path = self.debug_dir.join(filename);
		if let Some(p) = path.parent()
		{
			report!(fs::create_dir_all(p).await)?;
		}
		if !append && path.is_file()
		{
			report!(fs::remove_file(&path).await)?;
		}
		let mut file = report!(
			fs::OpenOptions::new()
				.create(true)
				.write(true)
				.append(append)
				.open(&path)
				.await
		)
		.attach_printable_lazy(|| path.display().to_string())?;
		let mut writer = BufWriter::new(&mut file);
		while let Some(line) = lines.next().await
		{
			report!(writer.write_all(line.as_bytes()).await)?;
			report!(writer.write_all(&[b'\n']).await)?;
		}
		report!(writer.flush().await)?;
		Ok(())
	}

	/// reads file from debug directory
	pub async fn read_debug(&self, filename: &str) -> Result<Vec<String>>
	{
		let path = self.debug_dir.join(filename);
		let mut file = report!(fs::File::open(&path).await)?;
		let mut reader = LinesStream::new(BufReader::new(&mut file).lines());
		let mut contents = Vec::new();
		while let Some(line) = reader.next().await
		{
			contents.push(report!(line)?);
		}
		Ok(contents)
	}

	/// checks if some file exists in repository
	pub fn check_file_exists(&self, path: &str) -> bool
	{
		let git_path = self.git.get_path();
		let path = git_path.join(path);

		path.is_file()
	}

	pub async fn write_meta(&self, ta: &str) -> Result<()>
	{
		let meta = RepositoryMeta {
			commit_hash: self.hash.clone(),
			due: self.due.with_timezone(&crate::TZ).to_rfc2822(),
			ta: ta.to_string()
		};
		let lines = report!(serde_yaml::to_string(&meta))?;
		self.write_debug(
			"meta.yaml",
			stream::iter(lines.lines().map(|s| s.to_string())),
			false
		)
		.await?;
		Ok(())
	}
}
