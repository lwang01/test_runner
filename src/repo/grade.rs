use std::{
	collections::{HashMap, HashSet},
	path::PathBuf,
	time::{Duration, Instant}
};

use anyhow::anyhow;
use futures::{
	stream::{self, FuturesOrdered},
	StreamExt
};
use once_cell::sync::Lazy;
use regex::Regex;
use serde_json::Value;
use tokio::{fs, process::Command};
use tracing::log::warn;

use super::StudentRepo;
use crate::{
	drivers::docker::{
		container::{
			helper::{ContainerExt, ContainerStateStatusEnum},
			Container
		},
		network::{helper::NetworkExt, Network}
	},
	report,
	utils::{PathExt, Result, ToVecStrExt}
};

#[allow(clippy::unwrap_used)]
static RESOLVE_REGEX: Lazy<Regex> =
	Lazy::new(|| Regex::new(r#"\$\{.*?\}"#).unwrap());

/// TODO: Handle Grade vs Aggregate commands
pub struct GradingArgs
{
	pub student_tests_output_dir: PathBuf,
	pub student_repo_dir: PathBuf,
	pub score_output_name: Option<String>,
	pub comments_output_name: Option<String>,
	pub no_overwrite: Option<bool>,
	pub additional_args: Option<HashMap<String, Option<Value>>>
}

impl GradingArgs
{
	pub async fn build_arg_list(&self) -> Result<Vec<String>>
	{
		let mut arg_list: Vec<String> = Vec::new();
		let student_tests_output_dir =
			self.student_tests_output_dir.as_path().canonicalize_to_str()?;

		let student_repo_dir =
			self.student_repo_dir.as_path().canonicalize_to_str()?;

		arg_list.push("grade".to_owned());

		arg_list.push("--student-tests-output-dir".to_owned());
		arg_list.push(student_tests_output_dir);

		arg_list.push("--student-repo-dir".to_owned());
		arg_list.push(student_repo_dir);

		if let Some(score_output_name) = &self.score_output_name
		{
			arg_list.push("--score-output-name".to_owned());
			arg_list.push(score_output_name.to_owned());
		}

		if let Some(comments_output_name) = &self.comments_output_name
		{
			arg_list.push("--comments-output-name".to_owned());
			arg_list.push(comments_output_name.to_owned());
		}

		if let Some(no_overwrite) = self.no_overwrite
		{
			if no_overwrite
			{
				arg_list.push("--no-ovewrite".to_owned());
			}
		}

		if let Some(additional_args) = &self.additional_args
		{
			additional_args.iter().for_each(|(key, value)| {
				if let Some(val) = value
				{
					if val.is_boolean() && val.as_bool().unwrap_or(false)
					{
						arg_list.push(key.to_owned());
					}
					else if val.is_string() || val.is_number()
					{
						arg_list.push(key.to_owned());
						arg_list.push(val.to_string());
					}
					else
					{
						warn!("Grading argument value {} was not a boolean, string, or number", val);
					}
				}
				else
				{
					arg_list.push(key.to_owned());
				}
			});
		}

		Ok(arg_list)
	}
}

pub struct GradingAggregator
{
	pub assignment_output_dir: PathBuf,
	pub aggregated_output_dir: PathBuf,
	pub aggregated_output_filename: Option<String>,
	pub student_score_filename: Option<String>,
	pub no_overwrite: Option<bool>,
	pub no_student_info: Option<bool>,
}

impl GradingAggregator
{
	pub async fn build_arg_list(&self) -> Result<Vec<String>>
	{
		let mut arg_list: Vec<String> = Vec::new();
		let assignment_output_dir =
			self.assignment_output_dir.as_path().canonicalize_to_str()?;

		let aggregated_output_dir =
			self.aggregated_output_dir.as_path().canonicalize_to_str()?;

		arg_list.push("aggregate".to_owned());

		arg_list.push("--assignment-output-dir".to_owned());
		arg_list.push(assignment_output_dir);

		arg_list.push("--output-dir".to_owned());
		arg_list.push(aggregated_output_dir);

		if let Some(aggregated_output_filename) = &self.aggregated_output_filename
		{
			arg_list.push("--output-name".to_owned());
			arg_list.push(aggregated_output_filename.to_owned());
		}

		if let Some(student_score_filename) = &self.student_score_filename
		{
			arg_list.push("--score-file-name".to_owned());
			arg_list.push(student_score_filename.to_owned());
		}

		if let Some(no_overwrite) = self.no_overwrite
		{
			if no_overwrite
			{
				arg_list.push("--no-ovewrite".to_owned());
			}
		}

		if let Some(no_student_info) = self.no_student_info
		{
			if no_student_info
			{
				arg_list.push("--no-student-info".to_owned());
			}
		}

		Ok(arg_list)
	}

	pub async fn aggregate(&self, script_path: &PathBuf) -> Result<()>
	{
		let arg_list = self.build_arg_list().await?;
		// launch some external process to handle output and assign grades
		let mut cmd = report!(Command::new(script_path)
			.args(arg_list)
			.spawn())?;

		report!(cmd.wait().await)?;

		Ok(())
	}

}

#[async_trait::async_trait]
trait ResolveVecStrExt
{
	/// populate launch command in ContainerConfig by substituting relevant ip address
	async fn resolve(
		&self, container_name_prefix: &str, network: &Network
	) -> Result<Vec<String>>;
}

#[async_trait::async_trait]
impl ResolveVecStrExt for Vec<String>
{
	async fn resolve(
		&self, container_name_prefix: &str, network: &Network
	) -> Result<Vec<String>>
	{
		tracing::debug!({?self}, "Resolving Vec<String>");
		// fetch latest network information
		// notice type of this is not our Network, but bollard's Network
		let network: bollard::models::Network = network.inspect().await?;
		let mut resolved = Vec::new();
		for arg in self
		{
			let captures = RESOLVE_REGEX.captures_iter(arg).collect::<Vec<_>>();
			if captures.is_empty()
			{
				// if not dynamic, ignore
				resolved.push(arg.clone());
				continue;
			};

			// consolidate captures
			let mut captures_set = HashSet::new();
			#[allow(clippy::unwrap_used)]
			for capture in captures
			{
				// 0 is entire capture, so will always succeed
				captures_set.insert(capture.get(0).unwrap().as_str());
			}

			let mut arg = arg.clone();

			for capture in captures_set
			{
				// if ${}, resolve
				let capture =
					capture.trim_start_matches("${").trim_end_matches('}');

				let (container, variable) =
					report!(capture.split_once(':').ok_or_else(|| anyhow!(
						"Invalid variable format: {}",
						capture
					)))?;
				// special variables that doesn't reference other containers
				if variable == "name"
				{
					let name =
						format!("{}{}", container_name_prefix, container);
					arg = arg.replace(&format!("${{{}}}", capture), &name);
					continue;
				}
				let containers_in_network =
					report!(network.containers.as_ref().ok_or_else(|| {
						anyhow!("Network has no containers: {:?}", &network)
					}))?;
				// look for this container
				let mut network_container = None;
				// key of this map is container hash, so we just search through the values
				for net_container in containers_in_network.values()
				{
					// skip if container has no name (i.e. wiretapper)
					let container_name = if let Some(n) =
						net_container.name.clone()
					{
						n
					}
					else
					{
						continue;
					};

					let expected_name =
						format!("{}{}", container_name_prefix, container);
					if container_name == expected_name
					{
						network_container = Some(net_container);
						break;
					}
				}
				let network_container =
					report!(network_container.ok_or_else(|| {
						anyhow!(
							"Could not find container {}{} in network {:?}",
							container_name_prefix,
							container,
							&network
						)
					}))?;
				match &variable.to_ascii_lowercase()[..]
				{
					"ipv4" =>
					{
						let ipv4 = report!(network_container
							.ipv4_address
							.clone()
							.ok_or_else(|| {
								anyhow!(
									"Container {} has no ipv4 address: {:?}",
									container,
									&network_container
								)
							}))?;
						let ipv4 =
							ipv4.split('/').next().unwrap_or(&ipv4).to_string();
						arg = arg.replace(&format!("${{{}}}", capture), &ipv4);
					}
					"mac" =>
					{
						let mac = report!(network_container
							.mac_address
							.clone()
							.ok_or_else(|| {
								anyhow!(
									"Container {} has no mac address: {:?}",
									container,
									&network_container
								)
							}))?;
						arg = arg.replace(&format!("${{{}}}", capture), &mac);
					}
					_ =>
					{
						return report!(Err(anyhow!(
							"Invalid variable: {}",
							variable
						)))
					}
				}
			}
			resolved.push(arg);
		}
		tracing::debug!({?resolved}, "Resolved Vec<String>");
		Ok(resolved)
	}
}

impl StudentRepo
{
	/// build (runs make)
	///
	/// writes results to debug directory
	///
	/// TODO: verify makefile has -Wall
	///
	/// TODO: detect gcc warnings
	pub async fn build(&self) -> Result<()>
	{
		let container_name = format!(
			"{}-{}-builder",
			self.directory_id, self.assignment.config.repo_name
		);
		let mut container = Container::create(
			&container_name,
			&self.image_tag,
			self.git.get_path(),
			false,
			vec![],
			// convert Vec<String> to Vec<&str> - necessary due to bollard's weird generic choices
			self.assignment.config.build_command.to_vec_str(),
			None
		)
		.await?;
		container.start().await?;
		let succeeded = container
			.poll_for_status_with_timeout(
				ContainerStateStatusEnum::EXITED,
				Duration::from_secs(120)
			)
			.await?;
		if !succeeded
		{
			container.stop().await?;
			tracing::warn!("{}'s build timed out", self.directory_id);
		}

		let stdout = container.get_stdout().await?;
		let stderr = container.get_stderr().await?;
		self.write_debug("build.stdout", stdout, false).await?;
		self.write_debug("build.stderr", stderr, false).await?;
		container.teardown().await?;
		Ok(())
	}

	/// run test
	///
	/// writes results to debug directory
	pub async fn run_tests(&self) -> Result<()>
	{
		// for each test
		for (test_name, container_config) in &self.assignment.tests
		{
			let mut skip = false;
			// check expected artifacts
			for artifact in &container_config.expected_artifacts
			{
				if !self.check_file_exists(artifact)
				{
					tracing::warn!("{}'s test {} expects artifact {} but it does not exist, skipping", self.directory_id, test_name, artifact);
					skip = true;
					break;
				}
			}
			if skip
			{
				continue;
			}

			tracing::debug!({?container_config}, "Running test");

			// create network
			let network_name = format!(
				"{}-{}-{}",
				self.directory_id, self.assignment.config.repo_name, test_name
			);
			let network = Network::create(&network_name, false).await?;

			// setup tcpdump for network
			let sniffer = if container_config.tcpdump
			{
				let output_dir = &self.debug_dir.join(test_name);
				if !output_dir.is_dir()
				{
					report!(fs::create_dir_all(output_dir).await)?;
				}
				Some(network.start_tcpdump(output_dir, test_name).await?)
			}
			else
			{
				None
			};

			// create persistent containers
			let mut persistent_containers = Vec::new();
			for (name, config) in &container_config.layout
			{
				// skip ones with timeout
				if config.timeout > 0
				{
					continue;
				}

				let container_name_prefix = format!(
					"{}-{}-{}-",
					self.directory_id,
					self.assignment.config.repo_name,
					test_name,
				);
				let container_name =
					format!("{}{}", container_name_prefix, name);

				let mut additional_mounts = Vec::new();
				for (host_path, container_path, ro) in &config.additional_mounts
				{
					additional_mounts.push((
						self.assignment
							.tests_path
							.join(test_name)
							.join(host_path),
						&container_path[..],
						*ro
					));
				}

				let container = Container::create(
					&container_name,
					&self.image_tag,
					if config.reference
					{
						&self.reference_path
					}
					else
					{
						self.git.get_path()
					},
					config.read_only,
					additional_mounts,
					config
						.launch_cmd
						.resolve(&container_name_prefix, &network)
						.await?
						.to_vec_str(),
					Some(&network)
				)
				.await?;
				persistent_containers.push((container, name.clone()));
			}

			// persistent containers must be started before ephemeral containers are created,
			// otherwise resolving will fail
			// start persistent containers
			for (container, _) in &mut persistent_containers
			{
				container.start().await?;
				let running = container
					.poll_for_status_with_timeout(
						ContainerStateStatusEnum::RUNNING,
						Duration::from_secs(10)
					)
					.await?;
				if !running
				{
					tracing::warn!(
						"{}'s {} died prematurely",
						self.directory_id,
						container.get_name()?
					);
				}
			}

			// create ephemeral containers
			// container, name, timeout
			let mut ephemeral_containers = Vec::new();
			for (name, config) in &container_config.layout
			{
				if config.timeout <= 0
				{
					continue;
				}

				let container_name_prefix = format!(
					"{}-{}-{}-",
					self.directory_id,
					self.assignment.config.repo_name,
					test_name,
				);
				let container_name =
					format!("{}{}", container_name_prefix, name);
				let mut additional_mounts = Vec::new();
				for (host_path, container_path, ro) in &config.additional_mounts
				{
					additional_mounts.push((
						self.assignment
							.tests_path
							.join(test_name)
							.join(host_path),
						&container_path[..],
						*ro
					));
				}

				let container = Container::create(
					&container_name,
					&self.image_tag,
					if config.reference
					{
						&self.reference_path
					}
					else
					{
						self.git.get_path()
					},
					config.read_only,
					additional_mounts,
					config
						.launch_cmd
						.resolve(&container_name_prefix, &network)
						.await?
						.to_vec_str(),
					Some(&network)
				)
				.await?;
				ephemeral_containers.push((
					container,
					name.to_string(),
					config.timeout
				));
			}

			if container_config.simultaneous_launch
			{
				let mut tasks = FuturesOrdered::new();

				// to truly "simultaneously" launch, we need to spawn new tasks to make it multithreaded
				// this requires concurrency guarantees across threads, so we pass ownership into future and get it back
				// could use Arc<Mutex<>> but since there will be no contention, Arc<Mutex<>> would be unnecessary overhead
				for (mut container, n, t) in ephemeral_containers
				{
					tasks.push_back(tokio::spawn(async move {
						match container.start().await
						{
							Ok(_) => Ok((container, n, t)),
							Err(e) => Err((e, container, n, t))
						}
					}));
				}
				// wait for all spawned tasks to finish
				let results: Vec<_> = tasks.collect().await;
				// gain ownership of elements back and recreate ephemeral containers
				ephemeral_containers = Vec::new();
				for result in results
				{
					match report!(result)?
					{
						Ok(elem) => ephemeral_containers.push(elem),

						// we might want to clean up on failure in the future, so `tasks.push_back(tokio::spawn())` still returns ownership to everything even on failure
						// but for now, we'll ignore it
						Err((e, ..)) =>
						{
							tracing::warn!(
								"failed to start ephemeral container: {}",
								e
							);
							return Err(e);
						}
					}
				}
			}
			else
			{
				// non-simultaneous launch - necessary due to dynamic variable resolving mechanics
				// start ephemeral containers
				for (container, ..) in &mut ephemeral_containers
				{
					container.start().await?;
				}
			}

			// get timestamp
			let start_time = Instant::now();

			// print network info for debug
			tracing::debug!(
				"{}'s network info: {:?}",
				self.directory_id,
				network.inspect().await?
			);

			// sleep and stop each container that are not persistent simultaneously
			let mut tasks = FuturesOrdered::new();
			for (mut container, container_key, timeout) in ephemeral_containers
			{
				let directory_id = self.directory_id.clone();
				tasks.push_back(tokio::spawn(async move {
					let mut timed_out = false;
					let timeout_dur = Duration::from_secs(timeout as u64);
					let elapsed = start_time.elapsed();
					let remaining = timeout_dur.saturating_sub(elapsed);
					let finished = match container
						.poll_for_status_with_timeout(
							ContainerStateStatusEnum::EXITED,
							remaining
						)
						.await
					{
						Ok(f) => f,
						Err(e) => return Err((e, container, container_key))
					};
					if !finished
					{
						tracing::warn!(
							"{}'s {} timed out",
							directory_id,
							match container.get_name()
							{
								Ok(n) => n,
								Err(e) =>
								{
									return Err((e, container, container_key));
								}
							}
						);
						if let Err(e) = container.stop().await
						{
							return Err((e, container, container_key));
						}
						timed_out = true;
					}
					Ok((container, container_key, timeout, timed_out))
				}));
			}
			// wait for all spawned tasks to finish
			let results: Vec<_> = tasks.collect().await;
			// gain ownership of elements back and recreate ephemeral containers
			ephemeral_containers = Vec::new();
			for result in results
			{
				match report!(result)?
				{
					Ok((container, container_key, timeout, timed_out)) =>
					{
						if timed_out
						{
							self.write_debug(
								&format!(
									"{}/TIMEOUT-{}",
									test_name, container_key
								),
								stream::iter(vec!["timeout".to_string()]),
								false
							)
							.await?;
						}
						ephemeral_containers.push((
							container,
							container_key,
							timeout
						))
					}

					// we might want to clean up on failure in the future, so `tasks.push_back(tokio::spawn())` still returns ownership to everything even on failure
					// but for now, we'll ignore it
					Err((e, ..)) =>
					{
						tracing::warn!(
							"failed to stop ephemeral container: {}",
							e
						);
						return Err(e);
					}
				}
			}

			// stop persistent containers simultaneously
			let mut tasks = FuturesOrdered::new();
			for (mut container, container_key) in persistent_containers
			{
				let directory_id = self.directory_id.clone();
				tasks.push_back(tokio::spawn(async move {
					let properly_stopped = match container.stop().await
					{
						Ok(s) => s,
						Err(e) => return Err((e, container, container_key))
					};
					if !properly_stopped
					{
						tracing::warn!(
							"{}'s {} did stopped early",
							directory_id,
							match container.get_name()
							{
								Ok(n) => n,
								Err(e) =>
								{
									return Err((e, container, container_key));
								}
							}
						);
					}
					Ok((container, container_key))
				}));
			}
			// wait for all spawned tasks to finish
			let results: Vec<_> = tasks.collect().await;
			// gain ownership of elements back and recreate persistent containers
			persistent_containers = Vec::new();
			for result in results
			{
				match report!(result)?
				{
					Ok(elem) => persistent_containers.push(elem),
					Err((e, ..)) =>
					{
						tracing::warn!(
							"failed to stop persistent container: {}",
							e
						);
						return Err(e);
					}
				}
			}

			// write outputs for ephemeral containers
			for (container, container_key, _) in &mut ephemeral_containers
			{
				let stdout = container.get_stdout().await?;
				let stderr = container.get_stderr().await?;
				self.write_debug(
					&format!("{}/{}.stdout", test_name, container_key),
					stdout,
					false
				)
				.await?;
				self.write_debug(
					&format!("{}/{}.stderr", test_name, container_key),
					stderr,
					false
				)
				.await?;
			}

			// write outputs for persistent containers
			for (container, container_key) in &mut persistent_containers
			{
				let stdout = container.get_stdout().await?;
				let stderr = container.get_stderr().await?;
				self.write_debug(
					&format!("{}/{}.stdout", test_name, container_key),
					stdout,
					false
				)
				.await?;
				self.write_debug(
					&format!("{}/{}.stderr", test_name, container_key),
					stderr,
					false
				)
				.await?;
			}

			// remove containers
			for (container, _) in persistent_containers
			{
				container.teardown().await?;
			}
			for (container, ..) in ephemeral_containers
			{
				container.teardown().await?;
			}

			if let Some(s) = sniffer
			{
				s.teardown().await?;
			}
			network.teardown().await?;
		}

		Ok(())
	}

	/// grade based on test results
	pub async fn grade(&self, grading_args: &GradingArgs) -> Result<()>
	{
		let arg_list = grading_args.build_arg_list().await?;
		// launch some external process to handle output and assign grades
		let mut cmd = report!(Command::new(&self.assignment.grader_path)
			.args(arg_list)
			.spawn())?;

		report!(cmd.wait().await)?;

		Ok(())
	}
}
